# SQL Injection 1 - Users_ID

## Usage
Go to *$IP/?page=member*. There we are asked to give a user ID to obtain its details.
If you try to put an incorrect value (An alphabetic value, a wildcard, a quote),
you get the following message **"You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '*' at line 1"**

This error message, besides showing that we can reach the SQL Database, gives us
one important clue in order to attack it : its type, that is MySQL.

We can then try to obtain the content of the different tables of the DB, using
the most common MySQL commands.

Firstly, we manage to see the full content of the printed table by using the command :
**1 = 1 or 1** in the ID search input.
This will show us the complete list of users. The last one is called '*Get the Flag*' !
We can suppose that these informations are contained in a table of several columns,
and that only the column *'First Name'* and *'Surname'* are printed.

In other Terms, here is our guess about the complete request that is done :
**SELECT 'FIRSTNAME', 'SURNAME' FROM USERS_TABLE WHERE 'ID' LIKE <YOUR_INPUT>**

What we have to do next is to find where this users table was contained in order
to print the rest of its content.
To do this, we use the **UNION** command to append some other tables we want to print.
But, we are limited by the number of initial printed columns, that is two.


Thus, we append to the ID request the default command to show the root table and column names:
**1 = 1 or 1 UNION SELECT table_name, column_name FROM information_schema.columns**

The results displays a lot of informations, but mainly show us a **users* table
containing a **user_id** column, but also a **first_name** and a **last_name** column.
We then print the last two columns of the  **users** table (contersign & Commentaires),
using this command :
**1 = 1 or 1 UNION SELECT Commentaire, countersign FROM users**

For the ID 5, we obtain the following infos :
*"First name: Decrypt this password -> then lower all the char. Sh256 on it and it's good !*
*Surname : 5ff9d0165b4f92b14994e5c685cdce28"*

Using a simple [decrypting website](https://md5decrypt.net/en/Sha256/),
we discover that the surname is **FortyTwo**, and following the instructions
we got the flag.


## Protection
* Most of the last framework are supposed to protect agains SQL Injections !
* Do not provide any error messages on production environments. Save error messages
 with a reference number to a backend storage such as a text file or database,
 then show this number and a static user-friendly error message to the user.
* The preferred option is to use a safe API, which avoids the use of the
interpreter entirely or provides a parameterized interface, or migrate to use
Object Relational Mapping Tools (ORMs).
* Use positive or "whitelist" server-side input validation. This is not a
complete defense as many applications require special characters, such as text
 areas or APIs for mobile applications.
* For any residual dynamic queries, escape special characters using the specific
 escape syntax for that interpreter.
* Use LIMIT and other SQL controls within queries to prevent mass disclosure of
records in case of SQL injection.

## More on the Subject...
* [MySQL SQL Injection Simple Cheat Sheet](http://pentestmonkey.net/cheat-sheet/sql-injection/mysql-sql-injection-cheat-sheet)
* [MySQL SQL Injection Detailed Cheat Sheet](https://www.asafety.fr/mysql-injection-cheat-sheet/#Injection_dans_une_chaine_de_caracteres)
* [Computerphile - Youtube Video](https://www.youtube.com/watch?v=ciNHn38EyRc)
