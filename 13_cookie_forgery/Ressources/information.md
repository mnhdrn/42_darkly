# Cookie Forgery / Manipulation

## Usage

Open your Developer tool, go to Application > Cookie, here will stand a cookie with
this value `68934a3e9455fa72420237eb05902327`.

It's a md5 encrypted value, if we decrypt it, it stand for : `false`.
Simply encrypt `true` in md5, `b326b5062b2f0e69046810717534cb09`
and change the cookie value.

Then reload the page and the flag will appear.

In real life situation, you make the website think you are admin.

## Protection

* Don't stock Admin authorisation through cookie, or put a more strong encryption
for example encrypt in md5 add a seal and encrypt in sha256
-> true
-> `b326b5062b2f0e69046810717534cb09 + BornToSecAdmin`
-> `4acda596aba8c97aadc4dc8f8404718319553b09df0dac3eda005e4d25a5973a`

* The most effective way to avoid DOM-based cookie manipulation vulnerabilities is not to dynamically
write to cookies using data that originated from any untrusted source.
This behavior should never be implemented for cookies that have any role in
controlling privileged actions or user sessions within the application.

## More on the Subject...
[CWE Tips on how to protect Cookies](https://cwe.mitre.org/data/definitions/565.html)
