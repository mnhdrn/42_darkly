# Survey Falsification

## Usage

Go to the Survey page. You will find a form with <select> balise.
Inspect the <select> element and change is <option> value to a number of your choice.
for example: __99999__.

This is an issue because it let inject false information into the survey.

## Protection

Verify the number sent by the form in the Back End, so if it's not correct,
return a error message for the user and doesn't count is survey entry.

## More on the Subject...
* [Simple Presentation](https://stackoverflow.com/questions/6080204/modify-html-before-submitting-form-prevent-this-hack)
* [Detailed](https://www.oreilly.com/library/view/html5-hacks/9781449335052/ch01.html)
