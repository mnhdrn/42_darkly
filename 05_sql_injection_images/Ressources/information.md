# SQL Injection 1 - Images_ID

## Usage
Go to *$IP/index.php?page=searchimg*. There we are asked to give a image ID to obtain its details.
Following the same steps as in the previous flag, we find that the title of the
image corresponding the ID 5 is '*Hack Me*'


Using the same commands as before, we reach a **list_images** table
containing a **comment** column. We can look into it with the command:
**1 or 1 = 1 union select title,comment from list_images**

For the ID 5, we obtain the following infos :
*"Title: If you read this just use this md5 decode lowercase then sha256 to win this flag ! : 1928e8083cf461a51303633093573c46*
*Url : Hack me ?"*

Using a simple [decrypting website](https://md5decrypt.net/en/Sha256/),
we discover that the title is **albatroz**, and following the instructions
we get the flag.

Yet, another table called '*db_default*', containing a password '*password*'column
piqued or interest...

## Protection
* Most of the last framework are supposed to protect agains SQL Injections !
* Do not provide any error messages on production environments. Save error messages
 with a reference number to a backend storage such as a text file or database,
 then show this number and a static user-friendly error message to the user.
* The preferred option is to use a safe API, which avoids the use of the
interpreter entirely or provides a parameterized interface, or migrate to use
Object Relational Mapping Tools (ORMs).
* Use positive or "whitelist" server-side input validation. This is not a
complete defense as many applications require special characters, such as text
 areas or APIs for mobile applications.
* For any residual dynamic queries, escape special characters using the specific
 escape syntax for that interpreter.
* Use LIMIT and other SQL controls within queries to prevent mass disclosure of
records in case of SQL injection.

## More on the Subject...
* [MySQL SQL Injection Simple Cheat Sheet](http://pentestmonkey.net/cheat-sheet/sql-injection/mysql-sql-injection-cheat-sheet)
* [MySQL SQL Injection Detailed Cheat Sheet](https://www.asafety.fr/mysql-injection-cheat-sheet/#Injection_dans_une_chaine_de_caracteres)
* [Computerphile - Youtube Video](https://www.youtube.com/watch?v=ciNHn38EyRc)
