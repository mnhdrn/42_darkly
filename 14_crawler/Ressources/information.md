# Using Robots.txt with a Crawler

## Usage

We used a homemade 'crawler' to look through the directories accessible as a robot,
which would not take into account the disallowance in the robots.txt file.
We then search through all the subdirectories of ./hidden, using find, cat and grep
with a regex corresponding to the previous flags.
Among the 35.000 files, we can find one corrsponding !

Here's the link to the file : __
http://<YOUR_VM_IP>/.hidden/whtccjokayshttvxycsvykxcfm/igeemtxnvexvxezqwntmzjltkt/lmpanswobhwcozdqixbowvbrhw/ __

## Protection
The robots.txt file is not itself a security threat, and its correct use can represent good practice for non-security reasons.
You should not assume that all web robots will honor the file's instructions.
Rather, assume that attackers will pay close attention to any locations identified in the file.
Do not rely on robots.txt to provide any kind of protection over unauthorized access.

## More on the Subject...
* [OWASP - Crawling Code](https://www.owasp.org/index.php/Crawling_Code)
* [OWASP - Crawler Project](https://www.owasp.org/index.php/OWASP/Training/OWASP_Code_Crawler_Project)
* [OWASP - Information Exposure](https://cwe.mitre.org/data/definitions/200.html)
